<?php

namespace App\Http\Controllers;
use App\User;
use App\Role;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Auth;

class EmpleadoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $datas=User::with('role', 'empresa')->where('empresa_id', Auth::user()->empresa_id)->get();
        $rows=User::where('empresa_id', 6)->get();
        $roles=Role::all();
        // $datas['role']=$datas->role->nombre;
        // dd($email);
        return view('empleados.index', compact('datas', 'rows', 'roles'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    public function registrar()
    {
        $data=Role::all();
        return view('empleados.registrar', compact('data'));
    }

    public function guardado(Request $request)
    {
        $data=$request->except('_token');
        $data['password']= Hash::make($data['password']);
        $data['empresa_id']=Auth::user()->empresa_id;
        User::create($data);
        return redirect('empleados');
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {       
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function eliminar($id)
    {
        $empresa=6;
        User::where('id',$id)->update(['empresa_id'=>$empresa]);
        return redirect()->back();
    }
    public function agregar(Request $request)
    {
        $id=$request->email;
        $role=$request->role;
        $empresa=Auth::user()->empresa_id;
        User::where('id',$id)->update(['empresa_id'=>$empresa, 'role_id'=>$role]);
        return redirect()->back();
    }
    public function role(Request $request)
    {
        $id=$request->id;
        $role=$request->role;
        // dd($role);
        User::where('id',$id)->update(['role_id'=>$role]);
        return redirect()->back();
    }
    public function cambiarfoto(Request $request)
    {
        $data = $request->all();
        $file = $data['foto'];
        $path = public_path('images');
        $name = $file->getClientOriginalName();
        if( $file->move($path,  $name) ){
            // dd($name);
            User::where('id', Auth::id())->update(['foto'=>$name]);
            return back();
        }
    }
    
}
