<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Ruta;
use App\Empresa;
use Auth;

class RutaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if (Auth::user()->role_id == 1) {
            $rows = Ruta::all();
            return view('rutas.index',compact('rows'));
        }else{
             $rows = Ruta::all()->where('empresa_id', Auth::user()->empresa_id);
             return view('rutas.index',compact('rows'));
        }  
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        if (Auth::user()->role_id == 1 || Auth::user()->role_id == 2) {

        $data = Empresa::all();
        return view('rutas.crear', compact('data'));

        }else{

        return redirect('rutas');

        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if (Auth::user()->role_id == 1 || Auth::user()->role_id == 2) {

        $data= $request->all();

        if (Auth::user()->role_id == 2) {
             $data['empresa_id'] = Auth::user()->empresa_id;
        }

        Ruta::create($data);
        return redirect('rutas'); 

        }else{
             return redirect('rutas');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $data = Ruta::find($id);
        return view('rutas.show', compact('data'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        if (Auth::user()->role_id == 1 || Auth::user()->role_id == 2) {

        $data = Ruta::find($id);
        $dataEmp = Empresa::all();
        return view('rutas.editar', compact('data','dataEmp'));

        }else{
            return redirect('rutas');
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        if (Auth::user()->role_id == 1 || Auth::user()->role_id == 2) {

        $newdata= $request->all();
        $data= Ruta::find($id);

            $data->update($newdata);
            return redirect('rutas');
        }else{
            return redirect('rutas');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if (Auth::user()->role_id == 1 || Auth::user()->role_id == 2) {
        Ruta::find($id)->delete();
        return redirect('rutas');
        }else{
            return redirect('rutas');
        }
    }
}
