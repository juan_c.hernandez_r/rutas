<?php

namespace App\Http\Controllers;

use App\User;
use App\Role;
use Illuminate\Http\Request;
use App\Http\Requests\UserRequest;
use Illuminate\Support\Facades\Hash;

class UserController extends Controller
{
    /**
     * Display a listing of the users
     *
     * @param  \App\User  $model
     * @return \Illuminate\View\View
     */
    public function index()
    {
        $datas=User::with('role','empresa')->get();
        $roles = Role::all();
        return view('usuarios.index', compact('datas', 'roles'));
    }
    public function perfil($id)
    {
        $data = User::find($id);
        $roles = Role::all();

        return view('profile.index', compact('id', 'data', 'roles'));
    }
    public function cambiarempresa(Request $request)
    {
        $id=$request->id;
        $role=$request->role;
        // dd($request->all());
        User::where('id',$id)->update(['role_id'=>$role]);
        return redirect()->back();
    }
}
