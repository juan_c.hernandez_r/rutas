<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Bus;
use App\Empresa;
use App\Licencia;
use Auth;

class AutobusController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if (Auth::user()->role_id == 1) {
            $rows = Bus::all();
            return view('buses.index', compact('rows'));
        }else{
            $rows = Bus::all()->where('empresa_id', Auth::user()->empresa_id);
            return view('buses.index', compact('rows'));
        }  
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        if (Auth::user()->role_id == 1 || Auth::user()->role_id == 2) {
            $rows= Empresa::all();
            $rowsl = Licencia::all();
            return view('buses.crear', compact('rows','rowsl'));
        }else{

            return redirect('autobuses');
        }
       
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if (Auth::user()->role_id == 1 || Auth::user()->role_id == 2) {

            $data = $request->all();
            $file = $request->file('foto');

            if (Auth::user()->role_id == 2) {
                $data['licencia_estado'] = 'Inactiva';
                $data['empresa_id'] = Auth::user()->empresa_id;
            }

            $path = public_path('images');
            $name = $file->getClientOriginalName();

            if( $file->move($path,  $name) ){
                $data['foto'] = $name;
                Bus::create($data);
                return redirect('autobuses');
            }

            return view('buses.index');
        }else{

            return redirect('autobuses');
        }
 
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        if (Auth::user()->role_id == 1 || Auth::user()->role_id == 2) {

        $rowsb = Bus::find($id);
        $rows = Empresa::all();
        $rowsl = Licencia::all();
        // dd($rowsb);
        return view('buses.editar', compact('rowsb', 'rows','rowsl'));

        }else{

            return redirect('autobuses');
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

        if (Auth::user()->role_id == 1 || Auth::user()->role_id == 2) {

        $newdata= $request->all();
        $data= Bus::find($id);

        if (isset($newdata['foto'])) {
            $file = $request->file('foto');
            $path = public_path('images');
            $name = $file->getClientOriginalName();


             if( $file->move($path,  $name) ){
                $newdata['foto'] = $name;
                $data->update($newdata);
                return redirect('autobuses'); 
            }

        }else{

            $data ->update($newdata);
            return redirect('autobuses'); 

        }
       
        

        }else{

            return redirect('autobuses');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if (Auth::user()->role_id == 1 || Auth::user()->role_id == 2) {

        Bus::find($id)->delete();
        return redirect('autobuses');

        }else{

            return redirect('autobuses');
        }
    }
}
