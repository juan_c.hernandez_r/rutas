<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Empresa extends Model
{
    protected $fillable = ['nombre', 'nit'];

     public function buses()
    {
        return $this->hasMany('App\bus');
    }

     public function rutas()
    {
        return $this->hasMany('App\Rutas');
    }
}
