<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            'nombre' => 'Camilo',
            'apellido' => 'Hernandez',
            'email' => 'juan@gmail.com',
            'email_verified_at' => now(),
            'password' => Hash::make('123456'),
            'telefono' => '3224354004',
            'empresa_id' => rand(1,4),
            'role_id' => rand(1,4),
            'created_at' => now(),
            'updated_at' => now()
        ]);
    }
}
