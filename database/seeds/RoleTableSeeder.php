<?php
use App\Role;
use Illuminate\Database\Seeder;

class RoleTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $role = new Role();
        $role->nombre = 'superadmin';
        $role->nombre_visible = 'Super Administrador';
        $role->descripcion = 'Administradores del software encargados de dar soporte';
        $role->save();

        $role = new Role();
        $role->nombre = 'admin';
        $role->nombre_visible = 'Administrador';
        $role->descripcion = 'Administrador de la empresa de transporte y tiene el control total sobre ella';
        $role->save();

        $role = new Role();
        $role->nombre = 'empleado';
        $role->nombre_visible = 'Empleado';
        $role->descripcion = 'Persona que trabaja dentro de la empresa y tiene algunas funciones';
        $role->save();

        $role = new Role();
        $role->nombre = 'conductor';
        $role->nombre_visible = 'Conductor';
        $role->descripcion = 'Persona que trabaja dentro de la empresa y conduce algunas rutas dentro de ella';
        $role->save();
    }
}
