<?php
use App\Licencia;
use Illuminate\Database\Seeder;

class LicenciaTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $licencia = new Licencia();
        $licencia->tipo = 'Mensual';
        $licencia->valor = 45000;
        $licencia->save();

        $licencia = new Licencia();
        $licencia->tipo = 'Semestral';
        $licencia->valor = 250000;
        $licencia->save();

        $licencia = new Licencia();
        $licencia->tipo = 'Anual';
        $licencia->valor = 480000;
        $licencia->save();
    }
}
