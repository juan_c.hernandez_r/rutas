<?php
use App\Empresa;
use Illuminate\Database\Seeder;

class EmpresaTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $empresa = new Empresa();
        $empresa->nombre = 'Ermita';
        $empresa->nit = '4552154541';
        $empresa->save();

        $empresa = new Empresa();
        $empresa->nombre = 'Papagayo';
        $empresa->nit = '4565232151';
        $empresa->save();

        $empresa = new Empresa();
        $empresa->nombre = 'Rio Cali';
        $empresa->nit = '8951245121';
        $empresa->save();

        $empresa = new Empresa();
        $empresa->nombre = 'Decepaz';
        $empresa->nit = '5421782124';
        $empresa->save();
    }
}
