<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateBusesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('buses', function (Blueprint $table) {
            $table->id();
            $table->string('placa');
            $table->string('capacidad');
            $table->string('cilindraje');
            $table->string('licencia_estado');
            $table->string('operacion_estado');
            $table->unsignedBigInteger('empresa_id');
            $table->unsignedBigInteger('ruta_id');

            $table->foreign('empresa_id')->references('id')->on('empresas');
            $table->foreign('ruta_id')->references('id')->on('rutas');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('buses');
    }
}
