@extends('layouts.app', ['activePage' => 'typography', 'titlePage' => __('Empresa')])
  {{-- @section('estilos')
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0/css/materialize.min.css">
  @endsection --}}
  @section('scripts')
  <script>
    $(document).ready(function() {
    $('.mdb-select').materialSelect();
    });
  </script>
  @endsection
@section('content')
<div class="content">
  <div class="container-fluid">
    <div class="card">
      <div class="card-header card-header-primary">
        <h4 class="card-title">Empleados</h4>
        <p class="card-category"></p>
      </div>
      <div class="card-body">
        <div id="empleados" class="container w-50">
        <form class="form" method="POST" action="{{ url('registrar-empleado') }}">
        @csrf
        <div class="card card-login card-hidden mb-3">
          <div class="card-header card-header-primary text-center">
            <h4 class="card-title"><strong>{{ __('Registrar') }}</strong></h4>
          </div>
          <div class="card-body ">
            <div class="bmd-form-group mb-2">
              <div class="input-group">
                <div class="input-group-prepend">
                  <span class="input-group-text">
                      <i class="material-icons">face</i>
                  </span>
                </div>
                <input type="text" name="nombre" required class="form-control" placeholder="{{ __('Nombre...') }}" value="{{ old('nombre') }}" required>
              </div>
            </div>
            <div class="bmd-form-group mb-1">
              <div class="input-group">
                <div class="input-group-prepend">
                  <span class="input-group-text">
                      <i class="material-icons">account_circle</i>
                  </span>
                </div>
                <input type="text" name="apellido" required class="form-control" placeholder="{{ __('Apellido...') }}" value="{{ old('apellido') }}" required>
              </div>
            </div>
            <div class="bmd-form-group mb-2">
              <div class="input-group">
                <div class="input-group-prepend">
                  <span class="input-group-text">
                      <i class="material-icons">shuffle</i>
                  </span>
                </div>
                <div class="form-group">
                  <select class="form-control" name="role_id" width="100%" required>
                    <option disabled selected>Seleccione el role para este usuario...</option>
                    @foreach ($data as $role)
                      @if ($role->nombre_visible != 'Super Administrador')  
                      <option value=" {{$role->id}} "> {{ ucfirst($role->nombre_visible)}} </option>
                      @endif
                    @endforeach
                  </select>
                </div>
            </div>
          </div>
          <div class="bmd-form-group mb-2">
            <div class="input-group">
              <div class="input-group-prepend">
                <span class="input-group-text">
                    <i class="material-icons">email</i>
                </span>
              </div>
              <input type="email" name="email" required class="form-control" placeholder="{{ __('Email...') }}" value="{{ old('email') }}" required>
            </div>
          </div>
          <div class="bmd-form-group mb-2">
            <div class="input-group">
              <div class="input-group-prepend">
                <span class="input-group-text">
                  <i class="material-icons">lock_outline</i>
                </span>
              </div>
              <input type="password" required name="password" id="password" class="form-control" placeholder="{{ __('Contraseña...') }}" required>
            </div>
          </div>
          <div class="card-footer justify-content-center">
            <button type="submit" class="btn btn-primary btn-link btn-lg">{{ __('Crear cuenta') }}</button>
          </div>
        </div>
      </form>
        </div>
      </div>
    </div>
  </div>
</div>
@endsection