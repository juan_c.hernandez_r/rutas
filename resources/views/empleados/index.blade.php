@extends('layouts.app', ['activePage' => 'typography', 'titlePage' => __('Typography')])

@section('estilos')
  <link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-beta.1/dist/css/select2.min.css" rel="stylesheet" />
  @endsection
  @section('scripts')
  <script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-beta.1/dist/js/select2.min.js"></script>  
  <script>
    $(document).ready(function() {
    $('.js-example-basic-single').select2();
    });
  </script>
  
  @endsection
@section('content')
<div class="content">
  <div class="container-fluid">
    <div class="card">
      <div class="card-header card-header-primary">
        <h4 class="card-title">Mis empleados</h4>
        <p class="card-category"></p>
      </div>
      <div class="card-body">
        <div id="autobuses">
          <div class="card-title">
            <div class="table-responsive">
              @if (Auth::user()->role_id==2)
              <button type="button" class="btn btn-info btn-sm float-right" data-toggle="modal" data-target="#exampleModal">
                Agregar empleado ya registrado
              </button>
              <a href=" {{url('registrar-empleado')}} " class="btn btn-success btn-sm float-left" >
                Registrar empleado
              </a> 
              @endif
              @if (Auth::user()->empresa_id==6)
                  <p>No has sido asginado a una empresa, por lo cual no puedes ver tus compañeros</p>
              @else
                <table class="table">
                    <thead class=" text-primary">
                        <tr>
                            <th>Nombre</th>
                            <th>Apellido</th>
                            <th>Email</th>
                            <th>Role</th>
                            <th>Registrado</th>
                            <th class="text-right">Acciones</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($datas as $key => $data)
                        <tr>
                            <td> {{$data->nombre}} </td>
                            <td> {{$data->apellido}} </td>
                            <td> {{$data->email}} </td>
                            <td> {{ ucfirst($data->role->nombre) }} </td>
                            <td> {{ date('d-H-y, h:i a', strtotime($data->created_at)) }}  </td>
                            <td class="td-actions text-right">
                              <a href=" {{url('perfil/'.$data->id)}} " rel="tooltip" class="btn btn-info ">
                                  <i class="material-icons">person</i>
                              </a>
                              @if (Auth::user()->role_id==2)
                              {{-- <a  rel="tooltip" class="btn btn-success ">
                                  <i class="material-icons">edit</i>
                              </a> --}}
                              <form class="d-inline" action=" {{url('eliminar-empleado/'.$data->id)}} " method="POST" autocomplete="off">
                                @csrf
                                <input type="hidden" name="_method" value="PATCH">
                                <button type="submit" rel="tooltip" class="btn btn-danger">
                                  <i class="material-icons">close</i>
                              </button>
                              </form>
                              @endif
                          </td>
                        </tr>   
                        @endforeach
                        
                    </tbody>
                </table>
                @endif
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<div class="modal fade" id="exampleModal"  role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Agregar un nuevo empleado</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body" style="overflow: hidden;">
        <form action=" {{url('agregar-empleado')}} " method="post" class="d-inline" autocomplete="off">
        <input type="hidden" name="_method" value="PATCH">
        @csrf
        <div class="mb-3">
          <select required class="js-example-basic-single form-control " style="width: 100%; margin: 10px;" name="email">
            <option value="" readonly >Seleccione el correo...</option>
            @foreach ($rows as $row)       
            <option value=" {{$row->id}} "> {{$row->email}} </option>
            @endforeach
          </select>
        </div>
        <div class="mb-2">
          <select required class="js-example-basic-single mb-2" style="width: 100%" name="role">
            <option value="" readonly >Seleccione el role...</option>
            @foreach ($roles as $role)  
            @if ($role->nombre_visible != 'Super Administrador')  
            <option value=" {{$role->id}} "> {{ ucfirst($role->nombre_visible)}} </option>
            @endif     
            @endforeach
          </select>
        </div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
          <button type="submit" class="btn btn-primary">Agregar</button>
        </div>
      </form>
      </div>
  </div>
</div>
@endsection