<div class="input-group mb-3">
    <div class="input-group-prepend">
      <span class="input-group-text">
          <i class="material-icons">emoji_transportation</i>
      </span>
    </div>
    <input required type="text" name="nombre" id="nombre" class="form-control" placeholder="Nombre..."
    value="{{ old('nombre',$data->nombre,'') }}" >
</div>
<div class="input-group mb-3">
    <div class="input-group-prepend">
      <span class="input-group-text">
          <i class="material-icons">widgets</i>
      </span>
    </div>
    <input required type="text" id="nit" name="nit" class="form-control" placeholder="Nit..."
    value="{{ old('nit',$data->nit, '') }}" >
</div>