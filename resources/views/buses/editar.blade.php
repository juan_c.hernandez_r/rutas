@extends('layouts.app', ['activePage' => 'typography', 'titlePage' => __('Typography')])

@section('content')
<div class="content">
  <div class="container-fluid">
    <div class="card">
      <div class="card-header card-header-primary">
        <h4 class="card-title">Autobuses</h4>
        <p class="card-category"></p>
      </div>
      <div class="card-body">
        <div id="autobuses">
          <div class="card-title">
            <h2>Editar Bus</h2>
            <br>
            
            <form action="{{url('autobuses/'.$rowsb->id)}}" method="post" enctype="multipart/form-data">
             @csrf
              
              <input type="hidden" name="_method" value="PATCH">

              <div class="form-group">
                <label for="placa">Placa</label>
                <input required value="{{$rowsb->placa}}" type="text" class="form-control" id="placa" name="placa">
              </div>

              <br>

              <div class="form-group">
                <label for="capacidad">Capacidad</label>
                <input required value="{{$rowsb->capacidad}}" type="text" class="form-control" id="capacidad" name="capacidad">
              </div>

              <br>

              <div class="form-group">
                <label for="capacidad">Cilindraje</label>
                <input required value="{{$rowsb->cilindraje}}" type="text" class="form-control" id="capacidad" name="cilindraje">
              </div>

              <br>

              <div class="input-group mb-3">
                <label for="capacidad">foto</label>
                <br>
                  <input value="{{$rowsb->foto}}" type="file" class="form-control" placeholder="Foto" name="foto">
                  <input value="{{$rowsb->foto}}" type="text" class="form-control">
                  <div class="input-group-append">
                    <div class="input-group-text">
                      <span class="fas fa-user"></span>
                    </div>
                  </div>
              </div>

              <br>

              <div class="form-group">
                <label for="empresa">Licencia</label>
                <select required class="form-control" data-style="btn btn-link" id="empresa" name="licencia_id">
                  <option value="" selected disabled>Seleccione...</option>

                  @foreach ($rowsl as $row)
                  @if($row->id == $rowsb->licencia_id)
                  <option value="{{$row->id}}" selected>{{$row->tipo}}</option>
                  @else
                  <option value="{{$row->id}}">{{$row->tipo}}</option>
                  @endif
                  @endforeach

                </select>
              </div>

              <br>

              <div class="form-group">
                <label for="operacion">Estado Operativo</label>
                 <select required class="form-control" data-style="btn btn-link" id="operacion" name="operacion_estado">
                  @if($rowsb->operacion_estado == "Activo")
                  <option value="{{$rowsb->operacion_estado}}" selected>{{$rowsb->operacion_estado}}</option>
                  <option value="Inactivo">Inactivo</option>
                  @else
                  <option value="{{$rowsb->operacion_estado}}" selected>{{$rowsb->operacion_estado}}</option>
                  <option value="Activo">Activo</option>
                  @endif
                </select>
              </div>

               @if(Auth::user()->role_id==1)

               <div class="form-group">
                <label for="licencia">Estado de la licencia</label>
                 <select required class="form-control" data-style="btn btn-link" id="licencia" name="licencia_estado">
                  <option value="{{$rowsb->licencia_estado}}" selected disabled>{{$rowsb->licencia_estado}}</option>
                  <option value="Activa">Activa</option>
                  <option value="Inactiva">Inactiva</option>
                </select>
              </div>

              <br>

               <div class="form-group">
                <label for="empresa">Empresa</label>
                <select required class="form-control" data-style="btn btn-link" id="empresa" name="empresa_id">
                  <option value="" selected disabled>Seleccione...</option>

                  @foreach ($rows as $row)
                  @if($row->id == $rowsb->empresa_id)
                  <option value="{{$row->id}}" selected>{{$row->nombre}}</option>
                   @else
                  <option value="{{$row->id}}">{{$row->nombre}}</option>
                  @endif
                  @endforeach

                </select>
              </div>

              @endif
              
              <br>

              <button type="submit" class="btn btn-success">Submit</button>
              <a href="{{url('autobuses')}}" class="btn btn-info float-right">Volver</a>
            </form>

          </div>
        </div>
      </div>
    </div>
  </div>
</div>
@endsection