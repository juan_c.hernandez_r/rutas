@extends('layouts.app', ['activePage' => 'typography', 'titlePage' => __('Rutas')])

@section('content')
<div class="content">
  <div class="container-fluid">
    <div class="card">
      <div class="card-header card-header-primary">
        <h4 class="card-title">Rutas</h4>
        <p class="card-category"></p>
      </div>
      <div class="card-body">
        <div id="autobuses">
          <div class="card-title">
            <h2>Mis Rutas</h2>
             @if (Auth::user()->role_id==1 || Auth::user()->role_id==2)
            <a href="{{url('rutas/create')}}" class="btn btn-info float-right">Crear</a>
            @endif
            <br>
            <br>
            <br>
            <br>

        <div class="row">
            @foreach ($rows as $row)
                

            <div class="col-md-4">
              <div class="card">
                  <div class="card-header card-header-text card-header-primary">
                    <div class="card-text">
                      <h4 class="card-title">{{$row->nombre}}</h4>
                    </div>
                  </div>
                  <div class="card-body">
                      <p class="card-text" style="font-size: 1.2em!important">{{$row->origen}} - {{$row->destino}}</p>

                      @if (Auth::user()->role_id==1)
                      <p class="card-text" style="font-size: 1.2em!important">Empresa: {{$row->empresa->nombre}}</p>
                      @endif

                    <div class="row mt-2"> 
                          
                      <a href="{{url('rutas/'.$row->id)}}" style="color: white" rel="tooltip" class="btn btn-info mr-2">
                            <i class="material-icons">room</i>
                      </a>

                       @if (Auth::user()->role_id==1 || Auth::user()->role_id==2)

                      <a href="{{ route('rutas.edit', $row->id) }}" rel="tooltip" class="btn btn-success mr-2" style="color: white">
                          <i class="material-icons">edit</i>
                      </a>

                      <form  action="{{url('rutas/'.$row->id)}}" method="post">
                          @csrf
                          <input type="hidden" name="_method" value="DELETE">
                          <button type="submit" rel="tooltip" class="btn btn-danger">
                            <i class="material-icons">close</i>
                          </button>
                        </form>
                      @endif

                    </div>
                      
                  </div>
              </div>
            </div>

            @endforeach

          </div>

            <br>
            <br>
            <br>

          
            

            <!-- <div id ="map"></div> -->
           <!--  <center>
              <iframe src="https://www.google.com/maps/embed?pb=!1m28!1m8!1m3!1d37888.39452277208!2d-76.5207555197337!3d3.464444001431202!3m2!1i1024!2i768!4f13.1!4m17!3e0!4m3!3m2!1d3.4681142!2d-76.5024966!4m3!3m2!1d3.4530784!2d-76.517989!4m3!3m2!1d3.4520074999999997!2d-76.5207356!4m3!3m2!1d3.4578333!2d-76.51309669999999!5e0!3m2!1ses!2sco!4v1590867717604!5m2!1ses!2sco" width="600" height="450" frameborder="0" style="border:0;" allowfullscreen="" aria-hidden="false" tabindex="0"></iframe>
            </center> -->
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
@endsection