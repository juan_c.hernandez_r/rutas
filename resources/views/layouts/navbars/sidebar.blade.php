<div class="sidebar" data-color="orange" data-background-color="white" data-image="{{ asset('material') }}/img/sidebar-1.jpg">
  <div class="logo">
    <a href="" class="simple-text logo-normal">
      {{ __('RutasWeb') }}
    </a>
  </div>
  <div class="sidebar-wrapper">
    <ul class="nav">
      <li class="nav-item{{ $activePage == 'autobuses' ? ' active' : '' }}">
        <a class="nav-link" href="{{ url('autobuses') }}">
          <i class="material-icons">directions_bus</i>
            <p>{{ __('Autobuses') }}</p>
        </a>
      </li>
      <li class="nav-item{{ $activePage == 'empleados' ? ' active' : '' }}">
        <a class="nav-link" href="{{ url('empleados') }}">
          <i class="material-icons">account_circle</i>
            <p>{{ __('Empleados') }}</p>
        </a>
      </li>
      @if (Auth::user()->role_id==1)
      <li class="nav-item{{ $activePage == 'usuarios' ? ' active' : '' }}">
        <a class="nav-link" href="{{ url('usuarios') }}">
          <i class="material-icons">supervised_user_circle</i>
            <p>{{ __('Usuarios') }}</p>
        </a>
      </li>
      @endif
      <li class="nav-item{{ $activePage == 'rutas' ? ' active' : '' }}">
        <a class="nav-link" href="{{ url('rutas') }}">
          <i class="material-icons">navigation</i>
            <p>{{ __('Rutas') }}</p>
        </a>
      </li>
      @if (Auth::user()->role_id==1)
      <li class="nav-item{{ $activePage == 'empresas' ? ' active' : '' }}">
        <a class="nav-link" href="{{ url('empresas') }}">
          <i class="material-icons">domain</i>
            <p>{{ __('Empresas') }}</p>
        </a>
      </li>
      @endif
      <li class="nav-item{{ $activePage == 'licencias' ? ' active' : '' }}">
        <a class="nav-link" href="{{ url('licencias') }}">
          <i class="material-icons">confirmation_number</i>
            <p>{{ __('Licencias') }}</p>
        </a>
      </li>
      <li class="nav-item{{ $activePage == 'navegar' ? ' active' : '' }}">
        <a class="nav-link" href="{{ url('navegacion') }}">
          <i class="material-icons">map</i>
            <p>{{ __('Navegacion') }}</p>
        </a>
      </li>
    </ul>
  </div>
</div>