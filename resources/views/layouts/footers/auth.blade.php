<footer class="footer">
  <div class="container-fluid">
    <div class="copyright float-center">
    &copy;
    <script>
        document.write(new Date().getFullYear())
    </script> by
    <a href="" target="_blank">UAO </a>Camilo Hernandez - Jason Herrera.
    </div>
  </div>
</footer>