@extends('layouts.app', ['activePage' => 'typography', 'titlePage' => __('Empresa')])

@section('content')
<div class="content">
  <div class="container-fluid">
    <div class="card">
      <div class="card-header card-header-primary">
        <h4 class="card-title">Actualizar empresa</h4>
        <p class="card-category"></p>
      </div>
      <div class="card-body">
        <div id="licencias" class="container w-50">
            <form action=" {{url('licencias/'.$data->id)}} " method="POST" autocomplete="off">
            @csrf
            <input type="hidden" name="_method" value="PATCH">
            <div class="box-body m-4">
                <div class="input-group mb-3">
                    <div class="input-group-prepend">
                        <span class="input-group-text">
                            <i class="material-icons">timeline</i>
                        </span>
                    </div>
                    <input type="text" name="tipo" class="form-control" placeholder="Tipo..."
                    value=" {{ old('tipo',$data->tipo,'') }} " >
                </div>
                <div class="input-group mb-3">
                    <div class="input-group-prepend">
                        <span class="input-group-text">
                            <i class="material-icons">monetization_on</i>
                        </span>
                    </div>
                    <input type="text" name="valor" class="form-control" placeholder="Precio..."
                    value=" {{ old('valor',$data->valor,'') }} " >
                    @include('includes.boton-form-editar', ['url'=>'licencias'])
                </div>
            </div>
            </form>
        </div>
      </div>
    </div>
  </div>
</div>
@endsection