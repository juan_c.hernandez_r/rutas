<?php

use Illuminate\Support\Facades\Route;

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();
// Route::get('roles', function(){
// 	return \App\Role::with('user')->get();
// });

Route::get('/home', 'HomeController@index')->name('home');
Auth::routes();

Route::get('/home', 'HomeController@index')->name('home')->middleware('auth');

Route::group(['middleware' => 'auth'], function () {

	Route::get('perfil/{id}', 'UserController@perfil');
	Route::patch('cambiar-empresa', 'UserController@cambiarempresa');

	Route::resource('autobuses', 'AutobusController');

	Route::resource('empleados', 'EmpleadoController');
	Route::patch('eliminar-empleado/{id}', 'EmpleadoController@eliminar');
	Route::patch('agregar-empleado', 'EmpleadoController@agregar');
	Route::get('registrar-empleado', 'EmpleadoController@registrar');
	Route::post('registrar-empleado', 'EmpleadoController@guardado');
	Route::patch('cambiar-role', 'EmpleadoController@role');
	Route::patch('cambiar-foto', 'EmpleadoController@cambiarfoto');

	Route::resource('rutas', 'RutaController');
	Route::resource('empresas', 'EmpresaController');
	Route::resource('licencias', 'LicenciaController');
	Route::resource('navegacion', 'NavegacionController');
});

Route::group(['middleware' => 'auth'], function () {
	Route::resource('usuarios', 'UserController');
	Route::get('profile', ['as' => 'profile.edit', 'uses' => 'ProfileController@edit']);
	Route::put('profile', ['as' => 'profile.update', 'uses' => 'ProfileController@update']);
	Route::put('profile/password', ['as' => 'profile.password', 'uses' => 'ProfileController@password']);
});

